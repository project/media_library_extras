<?php

namespace Drupal\media_library_extras;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\media_library\MediaLibraryState;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Contains various access control methods for the media library.
 */
final class Access implements ContainerInjectionInterface {

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  private $entityDisplayRepository;

  /**
   * Access constructor.
   *
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(EntityDisplayRepositoryInterface $entity_display_repository) {
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_display.repository')
    );
  }

  /**
   * Controls access to the "add media" form in the media library.
   *
   * @param \Drupal\media_library\MediaLibraryState $state
   *   The current state of the media library.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result for the "add media" form.
   */
  public function addFormAccess(MediaLibraryState $state): AccessResultInterface {
    $result = AccessResult::neutral();

    if ($state->getOpenerId() === 'media_library.opener.field_widget') {
      $parameters = $state->getOpenerParameters();
      $entity_type_id = $parameters['entity_type_id'];
      $bundle = $parameters['bundle'];
      $field_name = $parameters['field_name'];

      // @todo Allow this to use non-default form modes. Right now, due to a
      // limitation in core, we can only reliably use the default form mode.
      // @see media_library_extras_field_widget_third_party_settings_form()
      $form_display = $this->entityDisplayRepository->getFormDisplay($entity_type_id, $bundle);

      $component = $form_display->getComponent($field_name);
      if ($component) {
        $allow_create = NestedArray::getValue($component, ['third_party_settings', 'media_library_extras', 'create']);

        $result = AccessResult::forbiddenIf($allow_create === FALSE);
      }
      $result->addCacheableDependency($form_display);
    }
    // In Drupal 9.2.4 and later, MediaLibraryState has cacheability metadata.
    // To prevent this from breaking on sites which have patched core to enable
    // this access check but otherwise don't yet support cacheability of
    // MediaLibraryState, we need to check for this interface.
    if ($state instanceof CacheableDependencyInterface) {
      $result->addCacheableDependency($state);
    }
    return $result;
  }

}
