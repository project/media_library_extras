<?php

namespace Drupal\media_library_extras;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\media\MediaTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions for each media type for the media library.
 */
final class Permissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * MediaPermissions constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TranslationInterface $translation) {
    $this->entityTypeManager = $entity_type_manager;
    $this->setStringTranslation($translation);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('string_translation')
    );
  }

  /**
   * Returns an array of media type permissions.
   *
   * @return array
   *   The media type permissions.
   *
   * @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function getPermissions() {
    $permissions = [];

    // CKEditor plugin wasn't introduced until 8.8, ensure it exists.
    if (class_exists('\Drupal\media_library\Plugin\CKEditorPlugin\DrupalMediaLibrary')) {
      // Generate media permissions for all media types.
      $media_types = $this->entityTypeManager->getStorage('media_type')
        ->loadMultiple();

      /** @var \Drupal\media\MediaTypeInterface $type */
      foreach ($media_types as $type) {
        $permissions += $this->getPermissionsForMediaType($type);
      }
    }

    return $permissions;
  }

  /**
   * Returns a list of media permissions for a given media type.
   *
   * @param \Drupal\media\MediaTypeInterface $type
   *   The media type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  private function getPermissionsForMediaType(MediaTypeInterface $type) {
    $type_id = $type->id();

    return [
      "select $type_id media in wysiwyg media library" => [
        'title' => $this->t('Allow access to %type_name media in WYSIWYG media library', [
          '%type_name' => $type->label(),
        ]),
      ],
    ];
  }

}
