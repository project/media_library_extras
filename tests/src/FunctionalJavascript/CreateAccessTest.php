<?php

namespace Drupal\Tests\media_library_extras\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;

/**
 * @group media_library_extras
 */
class CreateAccessTest extends WebDriverTestBase {

  use EntityReferenceTestTrait;
  use MediaTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field_ui',
    'media_library_extras',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests the widget setting to toggle the "add media" form.
   */
  public function testCreateAccessSetting(): void {
    // We're testing a feature that only works in Drupal core 9.3 and up (added
    // in https://www.drupal.org/project/drupal/issues/3224530), so skip the
    // test if we're on an older version.
    $core_version = str_replace('-dev', NULL, \Drupal::VERSION);
    if (version_compare($core_version, '9.3.0', '<')) {
      $this->markTestSkipped('This test requires Drupal core 9.3 or later.');
    }

    $node_type = $this->drupalCreateContentType()->id();
    $this->createMediaType('image');
    $this->createEntityReferenceField('node', $node_type, 'field_media', 'Media', 'media');

    $this->container->get('entity_display.repository')
      ->getFormDisplay('node', $node_type)
      ->setComponent('field_media', [
        'type' => 'media_library_widget',
      ])
      ->save();

    $this->drupalLogin($this->rootUser);

    $this->drupalGet("/node/add/$node_type");
    $page = $this->getSession()->getPage();
    $page->pressButton('Add media');
    $assert_session = $this->assertSession();
    $this->assertNotEmpty($assert_session->waitForField('Add file'));

    $this->drupalGet("/admin/structure/types/manage/$node_type/form-display");
    $assert_session->pageTextContains('Allow media to be added');
    $page->pressButton('field_media_settings_edit');
    $checkbox = $assert_session->waitForField('Allow media to be added to the library');
    $this->assertNotEmpty($checkbox);
    $this->assertTrue($checkbox->isChecked());
    $checkbox->uncheck();
    $page->pressButton('Update');
    $this->assertTrue($assert_session->waitForText('Do not allow media to be added'));
    $page->pressButton('Save');
    $assert_session->pageTextContains('Your settings have been saved.');

    $component = $this->container->get('entity_display.repository')
      ->getFormDisplay('node', $node_type)
      ->getComponent('field_media');
    $this->assertFalse($component['third_party_settings']['media_library_extras']['create']);

    $this->drupalGet("/node/add/$node_type");
    $page->pressButton('Add media');
    $this->assertTrue($assert_session->waitForText('Add or select media'));
    $assert_session->fieldNotExists('Add file');
  }

}
