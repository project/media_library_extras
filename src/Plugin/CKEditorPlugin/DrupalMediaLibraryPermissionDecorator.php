<?php

namespace Drupal\media_library_extras\Plugin\CKEditorPlugin;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\editor\Entity\Editor;
use Drupal\ckeditor\Plugin\CKEditorPlugin\DrupalMediaLibrary;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Overrides the 'drupalmedialibrary' CKEditor plugin.
 *
 * This class uses the current user's permissions to limit which media types
 * are exposed in the media library when it is invoked via CKEditor.
 */
final class DrupalMediaLibraryPermissionDecorator extends DrupalMediaLibrary {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  private $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    // Add dependency locally to avoid affecting construct of parent.
    $instance->setCurrentUser($container->get('current_user'));

    return $instance;
  }

  /**
   * Sets the current user on the instance.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   */
  public function setCurrentUser(AccountProxyInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * Modify the current editor to reduce the allowed media types by permission.
   *
   * @param \Drupal\editor\Entity\Editor $editor
   *   The CKEditor Instance.
   *
   * @return array
   *   The CKEditor Config.
   */
  public function getConfig(Editor $editor) {
    if ($editor->hasAssociatedFilterFormat()) {
      $format = $editor->getFilterFormat();

      $embed_filter = $format->filters('media_embed');
      if ($embed_filter && $embed_filter->status) {
        $allowed_media_types = $embed_filter->settings['allowed_media_types'];
        // Store the previous setting so we can restore it later.
        $original_allowed_media_types = $allowed_media_types;

        // It's possible no items are selected on the filter, which means all.
        if (empty($allowed_media_types)) {
          $allowed_media_types = $this->mediaTypeStorage->getQuery()->execute();
        }
        $allowed_media_types = array_filter($allowed_media_types, function ($type_id) {
          return $this->currentUser->hasPermission("select $type_id media in wysiwyg media library");
        });

        // Set the new array of allowed types.
        $embed_filter->settings['allowed_media_types'] = $allowed_media_types;
      }
    }

    $config = parent::getConfig($editor);

    // Restore the original setting so that our momentary override does not
    // accidentally get saved.
    if (isset($original_allowed_media_types, $embed_filter)) {
      $embed_filter->settings['allowed_media_types'] = $original_allowed_media_types;
    }
    return $config;
  }

}
