<?php

namespace Drupal\Tests\media_library_extras\FunctionalJavascript;

use Behat\Mink\Element\NodeElement;
use Drupal\editor\Entity\Editor;
use Drupal\filter\Entity\FilterFormat;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\ckeditor\Traits\CKEditorTestTrait;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;

/**
 * Tests modifications to Media Library's CKEditor integration.
 *
 * @group media_library_extras
 */
class CKEditorTest extends WebDriverTestBase {

  use CKEditorTestTrait;
  use MediaTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'ckeditor',
    'media_library',
    'media_library_extras',
    'node',
    'text',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create the sample types to be embedded.
    $this->createMediaType('image', ['id' => 'image', 'label' => 'Image']);
    $this->createMediaType('image', ['id' => 'svg', 'label' => 'SVG']);
    $this->createMediaType('image', ['id' => 'animated', 'label' => 'Animated']);

    // Create a filter format that allows all media types.
    /** @var \Drupal\filter\FilterFormatInterface $format */
    $format = FilterFormat::create([
      'format' => 'test_format',
      'name' => 'Test format',
      'filters' => [
        'media_embed' => [
          'status' => TRUE,
          'settings' => [
            'default_view_mode' => 'view_mode_1',
            'allowed_media_types' => [],
            'allowed_view_modes' => [
              'view_mode_1' => 'view_mode_1',
              'view_mode_2' => 'view_mode_2',
            ],
          ],
        ],
      ],
    ]);
    $format->save();

    // Create the editor to house the format.
    Editor::create([
      'editor' => 'ckeditor',
      'format' => 'test_format',
      'settings' => [
        'toolbar' => [
          'rows' => [
            [
              [
                'name' => 'Main',
                'items' => [
                  'Source',
                  'Undo',
                  'Redo',
                ],
              ],
            ],
            [
              [
                'name' => 'Embeds',
                'items' => [
                  'DrupalMediaLibrary',
                ],
              ],
            ],
          ],
        ],
      ],
    ])->save();

    $this->drupalCreateContentType(['type' => 'blog']);
  }

  /**
   * Tests that the media library filters its allowed types by permissions.
   */
  public function testAllowedMediaTypesRestrictedByPermission() {
    // Allow the user to use image and animated, but not svg.
    $user = $this->drupalCreateUser([
      'use text format test_format',
      'access media overview',
      'create blog content',
      'select image media in wysiwyg media library',
      'select animated media in wysiwyg media library',
    ]);
    $this->drupalLogin($user);
    $this->assertSame(['Animated', 'Image'], $this->getExposedMediaTypes());

    $this->drupalLogout();

    $this->drupalLogin($this->rootUser);
    $this->assertSame(['Animated', 'Image', 'SVG'], $this->getExposedMediaTypes());
  }

  /**
   * Returns the names of the media types exposed in the media library.
   *
   * @return string[]
   *   The names of the media types exposed in the media library, sorted
   *   alphabetically.
   */
  private function getExposedMediaTypes() {
    $assert_session = $this->assertSession();

    // Open the media library in CKEditor, and verify the expected behavior.
    $this->drupalGet('/node/add/blog');
    $this->waitForEditor();
    $this->pressEditorButton('drupalmedialibrary');
    $this->assertNotEmpty($assert_session->waitForId('media-library-wrapper'));

    $types = $assert_session->elementExists('css', '.js-media-library-menu')
      ->findAll('css', 'a');

    $map = function (NodeElement $link) {
      return $link->getAttribute('data-title');
    };
    $types = array_map($map, $types);
    sort($types);

    return $types;
  }

}
